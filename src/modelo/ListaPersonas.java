/*
 * Clase para utilizar el polimorfismo con entrenadores y jugadores
 * Englobamos todo el personal
 */
package modelo;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import org.jdesktop.observablecollections.ObservableCollections;
import org.jdesktop.observablecollections.ObservableList;

/**
 *
 * @author mfontana
 */
public class ListaPersonas implements Serializable {
    
    private ObservableList<Persona> lista;

    public ListaPersonas() {
        lista = ObservableCollections.observableList(new ArrayList<Persona>());
    }

    public void altaPersona(Persona p) {
        lista.add(p);
    }
    
    public void bajaPersona(Persona p) {
        lista.remove(p);
    }
    
    public static final String PROP_LISTA = "lista";

    public ObservableList<Persona> getLista() {
        return lista;
    }

    public void setLista(ObservableList<Persona> lista) {
        ObservableList<Persona> oldLista = this.lista;
        this.lista = lista;
        propertyChangeSupport.firePropertyChange(PROP_LISTA, oldLista, lista);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    
}
